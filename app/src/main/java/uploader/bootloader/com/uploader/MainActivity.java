package uploader.bootloader.com.uploader;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private Button buttonSelect;
    private Button buttonUpload;
    private TextView textView;
    private Intent intent;
    private String path="";
    private BluetoothAdapter myBluetooth = null;
    private BluetoothSocket btSocket = null;
    private FileInputStream inputStream;
    private boolean enableThread = true;

    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address;

    private ListView lstvw;
    private ArrayAdapter aAdapter;
    private Thread thread;
    private BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
    private InputStream input;
    private OutputStream output;
    private Context getContextLocal(){
        return MainActivity.this;
    }
    private Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        enableThread = true;
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if(myBluetooth == null){
            //Show a mensag. that thedevice has no bluetooth adapter
            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
            //finish apk
            finish();
        }
        else{
            if (myBluetooth.isEnabled())
            { }
            else
            {
                Log.e("error","ebnable");
                //Ask to the user turn the bluetooth on
                Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnBTon,1);
            }
        }
        buttonSelect = (Button)findViewById(R.id.button_select);
        buttonUpload = (Button)findViewById(R.id.button_upload);
        textView = (TextView)findViewById(R.id.textView);
        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                startActivityForResult(intent, 7);
            }
        });
        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                address = "";
                prepareInterface();
            }
        });
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                upload();
            }
        });
        thread.start();
        handler = new Handler(getMainLooper());
    }

    private void upload(){
        Log.e("thread","started");
        while(enableThread){
            Log.e("thread","tes");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(btSocket != null && isBtConnected){
                File file = new File("/storage/emulated/0/Download/newFirmware.binary");
                if(!file.exists()){
                    Toast.makeText(getContextLocal(), "File doesnt exist",Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    output.write(new byte[]{'B',0,'E'});
                    output.flush();
                    boolean findFlag = false;
                    Log.e("state","sending request");
                    long timer = System.currentTimeMillis()+5000;
                    while(!findFlag){
                        if(timer < System.currentTimeMillis()){
                            output.write(new byte[]{'B',0,'E'});
                            timer = System.currentTimeMillis()+5000;
                        }
                        if(input.available()>0){
                            byte b[] = new byte[input.available()];
                            int res = input.read(b);
                            for(byte bb:b){
                                if(bb == 'O' || bb == (byte)0xFF){
                                    findFlag = true;
                                    Log.e("state","initiate bootloader");
                                }
                            }
                        }

                    }
                    Log.e("state","reading file");
                    inputStream = new FileInputStream(file);
                    byte buff[] = new byte[19];
                    int i = 0;
                    int count = 0;
                    while(inputStream.available()>0){
                        buff[i+2] = (byte)inputStream.read();
                        if(i>=15){
                            buff[0] = 'A';
                            buff[1] = 16;
                            buff[i+3] = 'E';
                            System.out.print(count+ " ");
                            for(byte x=0;x<19;x++)System.out.print(String.format("%02X ", buff[x]));
                            System.out.println();
                            count++;
                            i = 0;
                            output.write(buff);
                            output.flush();
                            findFlag = false;
                            while(findFlag){
                                if(input.available()>0) {
                                    byte b[] = new byte[input.available()];
                                    int res = input.read(b);
                                    for (byte bb : b) {
                                        if (bb == 'B') {
                                            findFlag = true;
                                            System.out.println("raw sent");
                                        }
                                    }
                                    if (!findFlag){
                                        output.write(buff);
                                    }
                                }
                                Thread.sleep(10);
                            }
                            Thread.sleep(10);
                            Arrays.fill(buff, (byte)0, (byte)19, (byte)0);
                        }else{
                            i++;
                        }
                        Thread.sleep(10);
                    }
                    if(i>0){
                        buff[0] = 'A';
                        buff[1] = (byte)(i);
                        buff[i+2] = 'E';
                        byte data[] = Arrays.copyOfRange(buff, 0, i+3);
                        for(byte x=0;x<(i+3);x++)System.out.print(String.format("%02X ", data[x]));
                        i = 0;
                        System.out.println("");
                        output.write(data);
                        findFlag = false;
                        while(findFlag){
                            if(input.available()>0) {
                                byte b[] = new byte[input.available()];
                                int res = input.read(b);
                                for (byte bb : b) {
                                    if (bb == 'B') {
                                        findFlag = true;
                                        System.out.println("last sent");
                                    }
                                }
                                if (!findFlag){
                                    output.write(buff);
                                }
                            }
                            Thread.sleep(10);
                        }
                    }
                    output.write(new byte[]{'A',0,'E'});
                    Thread.sleep(100);
                    while(input.available()<=0){
                        Thread.sleep(10);
                    }

                    while(true){
                        if(input.available()>0) {
                            byte data1 = (byte) input.read();
                            System.out.println("response " + String.format("%02X", data1));
                            if(data1==0x45){

                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        textView.setText("done");
                                    }
                                },100);
                                btSocket.close();
                                btSocket = null;
                                break;
                            }
                        }
                        Thread.sleep(10);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    private void prepareInterface(){
        Set pairedDevices = myBluetooth.getBondedDevices();
        ArrayList<String> list = new ArrayList<>();

        if (pairedDevices.size()>0) {
            for(Object b : pairedDevices) {
                BluetoothDevice bt = (BluetoothDevice)b;
                list.add(bt.getName() + "\n" + bt.getAddress()); //Get the device's name and the address
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
        }

        final CharSequence[] items = list.toArray(new CharSequence[list.size()]);//{ "Android", "Blackberry", "iOS", "Windows Phone" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContextLocal());
        builder.setTitle("Mobile Application");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String tempAddress = items[item].toString();
                address = tempAddress.substring(tempAddress.indexOf("\n")+1,tempAddress.length());
                Toast.makeText(getContextLocal(), address, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                executeBluetooth();
            }
        }).show();
    }

    public void executeBluetooth(){
        if(!address.equals("")){
            new ConnectBT().execute();
        } else {
            Log.e("choose","failed "+address);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub

        switch(requestCode){

            case 7:

                if(resultCode==RESULT_OK){

                    String PathHolder = data.getData().getPath();

                    Toast.makeText(MainActivity.this, PathHolder+new String(File.separator) , Toast.LENGTH_LONG).show();
                    Log.e("separator", File.separator);

                    path = PathHolder;
                    textView.setText(path);
                }
                break;

        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        Disconnect();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    private void Disconnect() {
        if(thread!=null){
            thread.interrupt();
            enableThread = false;
            thread = null;
        }
        if (btSocket!=null) {
            try{
                btSocket.close(); //close connection
                btSocket = null;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Disconnect();
    }

    private void startThread(){
        thread.start();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>{
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                    input = btSocket.getInputStream();
                    output = btSocket.getOutputStream();
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                Toast.makeText(getContextLocal(),"Connection Failed",Toast.LENGTH_LONG).show();
                Log.e("connection","failed");
                isBtConnected = false;
            }
            else
            {
                Toast.makeText(getContextLocal(),"Connection Established",Toast.LENGTH_LONG).show();
                Log.e("connection","succed");
                isBtConnected = true;
            }
        }
    }

}
